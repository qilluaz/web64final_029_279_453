-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: mariadb
-- Generation Time: Apr 03, 2022 at 06:05 PM
-- Server version: 10.7.3-MariaDB-1:10.7.3+maria~focal
-- PHP Version: 8.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Home`
--

-- --------------------------------------------------------

--
-- Table structure for table `CatHouse`
--

CREATE TABLE `CatHouse` (
  `CatHouseID` int(11) NOT NULL,
  `OwnerName` varchar(100) NOT NULL,
  `CatName` varchar(100) NOT NULL,
  `CatSpecies` varchar(100) NOT NULL,
  `CatSex` varchar(100) NOT NULL,
  `CatAge` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `DogHouse`
--

CREATE TABLE `DogHouse` (
  `HouseID` int(11) NOT NULL,
  `OwnerName` varchar(100) NOT NULL,
  `DogName` varchar(100) NOT NULL,
  `DogSpecies` varchar(100) NOT NULL,
  `DogSex` varchar(100) NOT NULL,
  `DogAge` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `HomeSeeker`
--

CREATE TABLE `HomeSeeker` (
  `SeekerID` int(11) NOT NULL,
  `SeekerName` varchar(100) NOT NULL,
  `SeekerSurname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `Patron`
--

CREATE TABLE `Patron` (
  `PatronID` int(11) NOT NULL,
  `PatronName` varchar(100) NOT NULL,
  `PatronSurname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `Success`
--

CREATE TABLE `Success` (
  `SuccessID` int(11) NOT NULL,
  `OwnerID` int(100) NOT NULL,
  `PatronID` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `CatHouse`
--
ALTER TABLE `CatHouse`
  ADD PRIMARY KEY (`CatHouseID`);

--
-- Indexes for table `DogHouse`
--
ALTER TABLE `DogHouse`
  ADD PRIMARY KEY (`HouseID`);

--
-- Indexes for table `HomeSeeker`
--
ALTER TABLE `HomeSeeker`
  ADD PRIMARY KEY (`SeekerID`);

--
-- Indexes for table `Patron`
--
ALTER TABLE `Patron`
  ADD PRIMARY KEY (`PatronID`);

--
-- Indexes for table `Success`
--
ALTER TABLE `Success`
  ADD PRIMARY KEY (`SuccessID`),
  ADD KEY `OwnerID` (`OwnerID`),
  ADD KEY `PatronID` (`PatronID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `CatHouse`
--
ALTER TABLE `CatHouse`
  MODIFY `CatHouseID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `DogHouse`
--
ALTER TABLE `DogHouse`
  MODIFY `HouseID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `HomeSeeker`
--
ALTER TABLE `HomeSeeker`
  MODIFY `SeekerID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Patron`
--
ALTER TABLE `Patron`
  MODIFY `PatronID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Success`
--
ALTER TABLE `Success`
  MODIFY `SuccessID` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Success`
--
ALTER TABLE `Success`
  ADD CONSTRAINT `OwnerID` FOREIGN KEY (`OwnerID`) REFERENCES `HomeSeeker` (`SeekerID`),
  ADD CONSTRAINT `PatronID` FOREIGN KEY (`PatronID`) REFERENCES `Patron` (`PatronID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
