const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')

const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET

const mysql = require('mysql')

const connection = mysql.createConnection({
    host : 'localhost',
    user : 'home',
    password :'home',
    database : 'Home'

})

connection.connect()

const express = require('express')
const { hash } = require('bcrypt')
const app = express()
const port = 4000

function authenticateToken(req, res, next) {
    const authHeader = req.headers['autorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET, (err, user) => {
        if (err) { return res.sendStatus(403)}
        else {
            req.user = user
            next()
        }
    })
}


/* API for Processing Buffeter Authorization*/
app.post("/login", (req, res) => {

    let username = req.query.username
    let use_password = req.query.password
    let query = `SELECT * FROM HomeSeeker WHERE Username= '${username}'`
    connection.query(query, (err, rows) =>{
        if (err){
            console.log(err)
            res.json({"status" : "400",
                         "message" : "Error querying from running db"  
                        })
        }else{
            let db_password = rows[0].Password
            bcrypt.compare(use_password, db_password, (err, result) => {
                if (result) {
                    let payload = {
                        "username" : rows[0].Username,
                        "user_id"  : rows[0].SeekerID,
                        "IsAdmin"  : rows[0].IsAdmin
                    }
                    console.log(payload)
                    let token = jwt.sign(payload, TOKEN_SECRET, { expiresIn : '1d'})
                    res.send(token)
                }else {
                    res.send("Invalid username / password")
                }
            })
        }
    })
})

app.post("/login2", (req, res) => {

    let username2 = req.query.username2
    let use_password2 = req.query.password2
    let query = `SELECT * FROM Patron WHERE Username= '${username}'`
    connection.query(query, (err, rows) =>{
        if (err){
            console.log(err)
            res.json({"status" : "400",
                         "message" : "Error querying from running db"  
                        })
        }else{
            let db_password = rows[0].Password
            bcrypt.compare(use_password, db_password, (err, result) => {
                if (result) {
                    let payload = {
                        "username" : rows[0].Username,
                        "user_id"  : rows[0].PatronID,
                        "IsAdmin"  : rows[0].IsAdmin
                    }
                    console.log(payload)
                    let token = jwt.sign(payload, TOKEN_SECRET, { expiresIn : '1d'})
                    res.send(token)
                }else {
                    res.send("Invalid username / password")
                }
            })
        }
    })
})



/* API for Registering a new Runner */
app.post("/register_user", authenticateToken, (req, res) => {
    let user_profile = req.user
    console.log(user_profile)

    let seeker_name = req.query.seeker_name
    let seeker_surname = req.query.seeker_surname
    let seeker_username = req.query.seeker_username
    let seeker_pass = req.query.seeker_pass

    bcrypt.hash(seeker_pass, SALT_ROUNDS, (err, hash) =>{
        let query = `INSERT INTO Buffeter (BuffeterName, BuffeterSurname, Username, Password, IsAdmin )
                VALUES ('${seeker_name}', '${seeker_surname}', '${seeker_username}', '${hash}', false)`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                        "status" : "400",
                        "message" : "Error inserting data into db"
                    })
        }
        else {
            res.json({
                        "status" : "200",
                        "message" : "Succesful"
            })
        }
    })
    } )

    
})

app.post("/add_dog", (req, res) => {

    let own_name = req.query.own_name
    let dog_name = req.query.dog_name
    let dog_spi = req.query.dog_spi
    let dog_sex = req.query.dog_sex
    let dog_age = req.query.dog_age

    let query = `INSERT INTO DogHouse (OwnerName, Dogname, DogSpicie, DogSex, DogAge )
                VALUES ('${own_name}', '${dog_namet}', '${dog_spi}', '${dog_sex}', '${dog_age}')`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                        "status" : "400",
                        "message" : "Error inserting data into db"
                    })
        }
        else {
            res.json({
                        "status" : "200",
                        "message" : "Succesful"
            })
        }
    })
})

app.post("/add_cat", (req, res) => {

    let own_name = req.query.own_name
    let cat_name = req.query.cat_name
    let cat_spi = req.query.cat_spi
    let cat_sex = req.query.cat_sex
    let cat_age = req.query.cat_age

    let query = `INSERT INTO CatHouse (OwnerName, Catname, CatSpicie, CatSex, CatAge )
                VALUES ('${own_name}', '${cat_namet}', '${cat_spi}', '${cat_sex}', '${cat_age}')`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                        "status" : "400",
                        "message" : "Error inserting data into db"
                    })
        }
        else {
            res.json({
                        "status" : "200",
                        "message" : "Succesful"
            })
        }
    })
})

app.get("/list_dog",( req, res ) => {
    let query = "SELECT * FROM DogHouse"
    connection.query( query,(err,rows) => {
                if (err){
                    res.json({"status" : "400",
                                 "message" : "Error querying from running db"  
                                })
                }else{
                    res.json(rows)
                }
    })
})

app.get("/list_cat",( req, res ) => {
    let query = "SELECT * FROM CatHouse"
    connection.query( query,(err,rows) => {
                if (err){
                    res.json({"status" : "400",
                                 "message" : "Error querying from running db"  
                                })
                }else{
                    res.json(rows)
                }
    })
})

app.get("/list_seeker",( req, res ) => {
    let query = "SELECT * FROM HomeSeeker"
    connection.query( query,(err,rows) => {
                if (err){
                    res.json({"status" : "400",
                                 "message" : "Error querying from running db"  
                                })
                }else{
                    res.json(rows)
                }
    })
})

app.get("/list_patron",( req, res ) => {
    let query = "SELECT * FROM Patron"
    connection.query( query,(err,rows) => {
                if (err){
                    res.json({"status" : "400",
                                 "message" : "Error querying from running db"  
                                })
                }else{
                    res.json(rows)
                }
    })
})

app.post("/update_cat", (req, res) => {

    let cat_id  = req.query.cat_id
    let owner    = req.query.owner
    let cat_name  = req.query.cat_name
    let cat_spi = req.query.cat_spi
    let cat_sex  = req.query.cat_sex
    let cat_age  = req.query.cat_age

    let query = `UPDATE CatHouse SET
                OwnerName = '${owner}',
                CatName = '${cat_name}',
                CatSpicie = '${cat_spi}'
                CatSex = '${cat_sex}',
                CatAge = '${cat_age}'
                WHERE  CatHouseID = '${cat_id}'`

    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                        "status" : "400",
                        "message" : "Error updating record"
                    })
        }
        else {
            res.json({
                        "status" : "200",
                        "message" : "Succesful"
            })
        }
    })
})

app.post("/delete_cat", (req, res) => {

    let cat_id    = req.query.cat_id

    let query = `DELETE FROM CatHouse 
                 WHERE CatHouseID = '${cat_id}'`

    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                        "status" : "400",
                        "message" : "Error deleting record"
                    })
        }
        else {
            res.json({
                        "status" : "200",
                        "message" : "Succesful"
            })
        }
    })
})

app.post("/delete_suc", (req, res) => {

    let suc_id    = req.query.suc_id

    let query = `DELETE FROM Success 
                 WHERE SuccessID = '${suc_id}'`

    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                        "status" : "400",
                        "message" : "Error deleting record"
                    })
        }
        else {
            res.json({
                        "status" : "200",
                        "message" : "Succesful"
            })
        }
    })
})





app.listen(port, () => {
    console.log(`Now starting BuffetReserv System Backend  ${port}`)
})




/*
query = "SELECT  * from Buffeter";
connection.query( query, (err, rows) => {
    if (err) {
        console.log(err);
    }
    else {
        console.log(rows);
    }
});

connection.end(); */